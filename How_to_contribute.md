# How to contribute

The final step before your work will be live.

<details><summary>

# Pull Requests</summary>
## [1. Edit what you're aiming at](How_to_edit.md)
## 2. Follow the Rules
- The project should be working before we add more or less useless functions
- Please either list all you have edited and which is not minor in your commit descriptions or in your pull request
- If needed and applicable, add further information like your goal or screenshots
- Commit in as less as possible commits but don't throw it all up because you made a fault and have to make a commit again or such
- Your edit should be optimised as much as possible, but we can also help you doing that afterwards
## 3. Upload it
Easier said than done.

### If you have no fork but a clone or download
let me shortly take a look in VSC...

Maybe there is a plugin doing this, but most probably you have to create a fork and then upload it per file upload into your fork repository.<br>Then you have to continue with the steps below.

### If you edited in the wiki (documentations)
Probably there is the possibility to also create a pull request like described below but I have no clue. If you have tested it out, please [send me](Contact.md) the results.

### If you have edited in a fork
you can easily open up a pull request in the original repository you have been working on.

[How to pull request](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/)

[Android](https://codeberg.org/Jikango/Android/compare/main...main) | [Frontier](https://codeberg.org/Jikango/Frontier/compare/main...main) | [WebApp](https://codeberg.org/Jikango/pages/compare/main...main) | [Desktop-App](https://codeberg.org/Jikango/pages/compare/desktop_app...main) | [API](https://codeberg.org/Jikango/Database_API/compare/main...main) | [commons](https://codeberg.org/Jikango/commons/compare/main...main)

--- 

## If you're having problems or need tips
There are [manuals](Manuals.md) and [useful links](Links.md).

---

</details>
<details><summary>

# Issues</summary>
Just stick to these rules:
- If you are new, please see [this](https://docs.codeberg.org/getting-started/issue-tracking-basics/)
- Please use a issue template if provided and not too off for your issue
- Please be as short and precise as possible. If there is no template, describe what exactly happened or what exactly you imagine. If you have an error, make sure to send it **without private data**
- It could need us some days to respond, so please be patient
- If there is no developer actively working on the project you opened an issue, it could need really long to get your issue done. (so how about learning to code in the meantime and fixing your own issue ;) )

[Android](https://codeberg.org/Jikango/Android/issues/new) | [Frontier](https://codeberg.org/Jikango/Frontier/issues/new) | [Web- & Desktop-App](https://codeberg.org/Jikango/pages/issues/new) | [API](https://codeberg.org/Jikango/Database_API/issues/new) | [commons](https://codeberg.org/Jikango/commons/issues/new)
</details>

---

# [Contact](Contact.md)