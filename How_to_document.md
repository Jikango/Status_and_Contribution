# How to document
> Sounds stupid but makes sense.

## 1st Step
> Understanding the code.

You have to fully know what is going on in order to be able to file a description or to say what a parameter does and what is needed for it.

As this is probably the hardest step, don't worry when you need a bit longer or even fail. Some I myself need an hour to understand my own code.

Ones you grabbed foot on what's going on, you can continue.

## 2nd Step
> Wiki Documentation

This needs to be done before in-code doc. to have the link for the in-code doc. 

**All of these start with**
```md
<div id="top"></div><- Previous page: 
[Previous Page](Previous-Page) <-<-<-
<br>-> Next page: 
[Next Page](Next-Page) ->->->->->->->

---

<br>
```
or similar. Here it is important again to create a nice image and tell the user as much in as less as possible.<br>This is also shown at the end of the page though without the `div id` and with `br` and `---` at the top.<br>
If you look through some of the [Database API Wiki](https://codeberg.org/Jikango/Database_API/wiki), I am sure you will understand what I mean.

So, now it depends on what you document, though there are these documentation styles right now:
> Please try and see if one of them fits for your need

<details><summary>How-to Documentation</summary>
Easiest said, built like all the pages here.

They are mostly built in steps and `details`, making it as easy as possible to get along with the content. They should link everything that is needed for accessibility but should not exaggerate on that. They should also be as informative, documenting everything the user needs to know if he doesn't know anything.

Also see [API/Getting Started](https://codeberg.org/Jikango/Database_API/wiki/Gettings-Started).

---

</details>
<details><summary>Request/Response Documentation</summary>
This one is a lot more complex and needs a lot more time.

As a general rule; if it gets too long, use details with `---` at the end.<br>
You start like described above and have to make sure, there is no information lost that should be known to the user.<br>
What you have to document, is every endpoint and every method of this endpoint. Also, don't forget the errors. You should probably use multiple files for this as it else gets too long.<br>
In these endpoint-methods or errors, there should be
- a description
- a request part which includes 
    - the buildup of the request 
        - with descriptions 
    - an example request
- a response part which includes
    - every possible response pattern
    - a description to the given values.

Also see [API/GET Requests](https://codeberg.org/Jikango/Database_API/wiki/GET-Requests).

---

</details>

<details><summary>Storing Documentation</summary>
For example how the Jikango Database is structured.

Normally, you should be able to do this with a table, if not, you have to be creative and think about another method to comprehensibly document this.

So, if you go with a table, you should split it up as per pattern. If we are talking about a database you are documenting, you split it up in `details` with each table. There you add the row
- `column` which you write in the name of every column in the table 
- `type`, so in which format it is stored
- `example`
- `notice` 
- as well as other neccessary rows.

Also see [API/Database Structure](https://codeberg.org/Jikango/Database_API/wiki/Database-Structure).

---

</details>

<details><summary>File/Function Documentation</summary>
This is where the code joins.

The probably best method is to split it up per file and/or folder. You should then maybe create a top wiki page for these groups where you link further sites. Also, you should replace the 'previous site, next site' buttons with 'back'.

In these files, you can define global rules and write the functions and classes below.

The general rule for functions, classes or properties is:
```md
### [async] [private] {class/function/property} name[(parameter1, parameter2[?])]: return-type-if-needed-with-link
```
The words in `[]` are optional and to be used when needed, you should select the fitting value of `{class/function/property}`, enter the name and afterwards optionally all parameters, if it is an optional parameter with `?` after it. At the end, enter the return value. `void` = nothing is returned.

Now that you have done that, if the function you are documenting is deprecated, add `***Deprecated***` below it and optionally a description why it is deprecated.

Hereafter follows the description of the class, function or property.

After that, create a table with all parameters of the **columns** 
- `parameter`
- `meaning` (of the parameter)
- `type` optionally with a link
- `example` for a given value which applies to as many cases which could be given as possible
- and `notice`.

Also see [API/Files and Functions - Models](https://codeberg.org/Jikango/Database_API/wiki/Files-and-Functions---Models).

---

</details>

<details><summary>(Sidebar and Footer)</summary>

**Sidebar**

If you create a fresh documentation, the sidebar should be built up in a way that the most important files or sites are listed there in a nice way.<br>
What I have done in the API wiki, is a folder like structure as a list, split up with `<br>`s.

Also see [API/Sidebar](https://codeberg.org/Jikango/Database_API/wiki/_Sidebar).

**Footer**

The footer should link to the top most important sites and the `top` anchor, if it is not even nearly identical with the API's.

Also see [API/Footer](https://codeberg.org/Jikango/Database_API/wiki/_Footer).

---

</details>

## 3rd Step
> In-code Documentation.

**Optional if you just document things like responses.**

As all languages we used support in-code documentation, this also is part of your job, if not already the programmer did the favor for you.

As for javascript, the basics are:
```js
/** Description
 * @param {type if appl.} [paramter] ([] stands for "optional") Description
 * ...
 * @returns {type} if needed
 * @see {@link documentation-header-link Documentation}
 */
```
For example:
```js
/** Stops the file execution synchronously if done correctly 
 * @param {number} ms The Milliseconds it's stopped
 * @see {@link https://codeberg.org/Jikango/Database_API/wiki/Files-and-Functions---The-Rest#function-sleep-ms-promise-nodejs-timeout Documentation}
*/
```

For a full documentation on this, refer to [here](https://jsdoc.app/).

## 4th Step
**[Load it up.](How_to_contribute.md)**

---

# [Contact](Contact.md)