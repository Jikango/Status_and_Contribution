# Contact

|    |
<a href='https://discord.gg/JZJ6jfHTdV'>
<img src='https://assets-global.website-files.com/6257adef93867e50d84d30e2/636e0a6a49cf127bf92de1e2_icon_clyde_blurple_RGB.png' width='140px'>
</a>
|    |
<a href='https://codeberg.org/Jikango/Status_and_Contribution/issues'>
<img src='https://design.codeberg.org/logo-kit/icon_inverted.svg' width='140px'>
</a>
|    |
<a href='https://matrix.org/try-matrix/'>
<img src='https://matrix.org/images/matrix-favicon.svg' width='140px'>
</a>
<br><br>

```
    Discord              Issues               Matrix
(Or: FileX#0375)                        (@filex:matrix.org)
```
---

<details>
<summary>Only the links</summary>

```
https://discord.gg/JZJ6jfHTdV 
 ===============================
https://codeberg.org/Jikango/Status_and_Contribution/issues
 ===============================
https://matrix.org/try-matrix
```
</details>