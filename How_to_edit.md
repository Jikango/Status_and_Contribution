# How to edit

This will lead you through the steps of editing or creating code in Jikango. (But not teach you how to code)

--- 

## If you're having problems or need tips
There are [manuals](Manuals.md) and [useful links](Links.md).

---

# 1. Choose your code editor
I recommend VS Codium, a fork of VSCode but there are many options of which the probably best are:
- [VS Codium](https://vscodium.com/)
- [VSCode / VSC](https://code.visualstudio.com/)
- [JetBrain's IDEs](https://www.jetbrains.com/)
- [Visual Studio](https://visualstudio.microsoft.com/)
- (Minimalistic: [Notepad++](https://notepad-plus-plus.org/))

# 2. [Download Git](https://git-scm.com/downloads)
Please really download it. It makes things ten times easier for you as well as for me.

# 3. Choose what you want to edit
Depending on it, you need different things.

### I would recommend you to fork them and afterwards clone this fork as you have to fork it either way.

### If you didn't come from there, you may firstly take a look at [the Basics](Jikango_Basics.md).

## Select your favorite:

<details><summary>

## The Documentations
</summary>

### 3.5. If this is your first time documenting with Jikango
Please actually read through [this](How_to_document.md). There could be things you are not aware of.

### 4. Fork or download the requirements
- The documentation of [Status & Contr.](https://codeberg.org/Jikango/Status_and_Contribution.wiki.git) | [the DB API](https://codeberg.org/Jikango/Database_API.wiki.git) | [the Web- & Desktop-App](https://codeberg.org/Jikango/pages.wiki.git) | [the Frontier](https://codeberg.org/Jikango/Frontier.wiki.git) | [the Android App](https://codeberg.org/Jikango/Android.wiki.git) | [the commons](https://codeberg.org/Jikango/commons.wiki.git)
- Plus the repository you want to document in = see the options below

### 5. Open both folders in your IDE
### 6. IF the part you document is working
You can skip step 5 on the end of the page. Else continue with step 5 on the end of the page.

---
</details>

<details><summary>

## The Web-/ Desktop-App
</summary>

### 4. Fork or download the requirements
- The [commons](https://codeberg.org/Jikango/commons/). If you want to do it correctly add them as dependecy to the project files
- [The Database API](https://codeberg.org/Jikango/Database_API.git) ([Download](https://codeberg.org/Jikango/Database_API/archive/main.zip)) (**YOU NEED TO [SET IT UP](https://codeberg.org/Jikango/Database_API/wiki/Getting-Started)**)
- [The Desktop-App](https://codeberg.org/Jikango/pages.git) ([Download](https://codeberg.org/Jikango/pages/archive/desktop_app.zip)) and the WebApp ([Download](https://codeberg.org/Jikango/pages/archive/main.zip))

### 5. Open the Folders of both Apps in your IDE

---
</details>

<details><summary>

## The Android App
</summary>

### 4. Fork or download the requirements
- [The Database API](https://codeberg.org/Jikango/Database_API.git) ([Download](https://codeberg.org/Jikango/Database_API/archive/main.zip)) (**YOU NEED TO [SET IT UP](https://codeberg.org/Jikango/Database_API/wiki/Getting-Started)**)
- [The Android App](https://codeberg.org/Jikango/Android.git) ([Download](https://codeberg.org/Jikango/Android/archive/main.zip))

### 5. Open the Android Folder in your IDE

---
</details>

<details><summary>

## The Frontier
</summary>

### 4. Fork or download the requirements
- If you are using JS: The [commons](https://codeberg.org/Jikango/commons/). If you want to do it correctly add them as dependecy to the project files
- [The Database API](https://codeberg.org/Jikango/Database_API.git) ([Download](https://codeberg.org/Jikango/Database_API/archive/main.zip)) (**YOU NEED TO [SET IT UP](https://codeberg.org/Jikango/Database_API/wiki/Getting-Started)**)
- [The Frontier](https://codeberg.org/Jikango/Frontier.git) ([Download](https://codeberg.org/Jikango/Frontier/archive/main.zip))

### 5. Open the Frontier Folder in your IDE

---
</details>

<details><summary>

## The API Database
</summary>

### 4. Fork or download the requirements
- The [commons](https://codeberg.org/Jikango/commons/). If you want to do it correctly add them as dependecy to the project files
- [The Database API](https://codeberg.org/Jikango/Database_API.git) ([Download](https://codeberg.org/Jikango/Database_API/archive/main.zip)) (**YOU NEED TO [SET IT UP](https://codeberg.org/Jikango/Database_API/wiki/Getting-Started)**)
- The [WebApp](https://jikango.codeberg.page/) to test it

### 5. Open the API Folder in your IDE

---
</details>

<details><summary>

### (The commons)
</summary>

Doesn't really need own developement but is rather developed with the other projects.<br>Though, if you want to only edit them (like I did in [NPG](https://codeberg.org/FileX/NPG)), then this may be half useful.

### 4. Check if your project is compatible with the commons

### 5. Fork or download the requirements
- The [commons](https://codeberg.org/Jikango/commons/). If you want to do it correctly add them as dependecy to the project files

---
</details>

<details><summary></summary>

### The Bot
There is neither a repsitory nor a beginning point. The bot would be to get information about your lessons on discord or other platforms.

However, this is just a project for once we got **all** the others up and running, so if this is done, please [contact](Contact.md) us.

If you want to get some started already, take a look at [VoiceActivity](https://codeberg.org/FileX/VoiceActivity).
</details>

## 6. Check that everything, including the DB API if needed, is running
If not, you may firstly have to develope what's not running or have a person that is doing that.
## 7. [Check what needs to be done](ToDos.md) or have your own idea
## 8. Edit it
## [9. Contribute it to the project](How_to_contribute.md)

---

# [Contact](Contact.md)