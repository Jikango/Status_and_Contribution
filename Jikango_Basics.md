<details>
<summary>   

# The founding time of the language</summary>
> While it is actually a program

So, I, FileX, was facing the final exam in high school and was told some years before you could do a project in IT ("CT", computer-technics). As I was into programming at that time since entering high school, so about 2 years, I was deciding to do that.<br>
Thinking about it, I got things like a small game on one of the microcontrollers or maybe some useful console application..

I chose a large project involving a whole schoolplanner.

Why? I couldn't think of a small console application and thought I couldn't just use an already started project as it would be unfair.<br>Which I regret in the end.

Also I was somewhat fascinated of open-source alternatives to those bad apps of money-companies.

So I put some friends and classmates, 3 of 4 couldn't really code, into place and we were setting it off with pretty much no one, including me, knowing what exactly to correctly do. 

So we met once in the holidays. There we were deciding the name from the candidates Polygo (I think), many other things including five and everything we could imagine of about school, and, since I was deeply into Japanese at that time, I finally came up with "Jikango". A combination of "Jikan", time, and "go", a language, just as I always think about mysterious things and involving to play with time. 
<br> So after that, we set the basics which were partly changing after time and tried to start. Everyone with the database. Which obviously was a mistake.<br>After some days or weeks I decided, we have to split up everyone in groups what they want to work and have weekly meetings. 
The formation was...
- Remedy and Lars preparing for their part of the apps
- Lolman (DELETE), Blackeleven (PATCH & PUT) and I (GET & Internal) are starting of the Database API

When we were 2 or 3 months into it, I finally finished the GET and internal part while Lolman and Blackeleven didn't start yet.<br>
I don't want to speak ill of others so I'll just tell you one was out after 5 months and I took over the whole Database API until the end as the other one seemed have too much to do. With the disappearance of the one, the frontier was cancelled and never started.

Our small presentation of the database API in March was pretty stressy to prepair as too many things were collapsing, but in the end, Blackeleven, Remedy and I received all best grades.

After that, about 3 weeks after, we tried to start working on the apps. As the person with seemingly too less time couldn't work on his app, the other one which would have to work under assistance of him also couldn't start and I started up the WebApp while my companion was forgetting what he had to do or just didn't have any time. 

After the final exam period, there we were, having 2 months to get it done.<br>Obviously impossible.

We had the GETs fully working as well as the DELETEs, the PATCHs, formerly POSTs, were running in the background but were too buggy to be used. The WebApp at this status was yet not done at all and some important pages were missing. As I and my companion could work in the CT lessons on it and I focussed more on the WebApp, we could however get the least down what was needed. 

So while I was trying to force the other team to finally start working, at least a bit, they couldn't get the ball running and were to stuck, so the time was gettings less and less. <br>
What I then finally did is to cancel their app, the Android App, and to move them to the documentation, where they, or at least Lars, did great work.

After that stressy two months it was finally time for the showdown.

### highest grade

And with this it all ended. The examinors seemed to be blown away and I swore to never code again.. once I'm done with all the crap I got left over to finish on other projects.

So that's pretty much the point you are at now. Except for what Lars continued on the documentation.

I am really thankful to everybody that helped and I will be really thankful if you can help out in this large project.<br>If you will need anything, I'll be there helping you for sure. Just contact me or one of the other developers at our [discord](https://discord.gg/JZJ6jfHTdV) (also linked at the end of this page or in [/contact](Contact.md)) or at FileX#0375.

### Thank you very much for reading this.
</details>

---

# Jikango Structure
## Basic Idea
The basic idea is just an open API server, connected to a database and accepting requests from client apps.

It shall...
- be Open-source
- have no tracking
- be free to use
- be a good user experience

If you simply want it in one word: **Free**.
<br>
<details>
<summary>   

## The Documentation</summary>
This includes the documentation for all projects.

As you will also have to edit in the code and understand it, knowledge of it is neccessary.

### Language
Markdown, the projects language

### Complexity
```
0 |+--------| 10
   1
```

In the end it's just writing some nice texts, creating tables and reading code.

### Finished
```
0 |---+-----| 10
      4
```

See the wikis of [Status & Contr.](https://codeberg.org/Jikango/Status_and_Contribution.wiki.git) | [the DB API](https://codeberg.org/Jikango/Database_API.wiki.git) | [the Web- & Desktop-App](https://codeberg.org/Jikango/pages.wiki.git) | [the Frontier](https://codeberg.org/Jikango/Frontier.wiki.git) | [the Android App](https://codeberg.org/Jikango/Android.wiki.git) | [the commons](https://codeberg.org/Jikango/commons.wiki.git).

### If you got interested,<br>
### [Read more](How_to_document.md)
<br>

If you want to contribute, continue with these two:
- [How to edit](How_to_edit.md)
- [How to contribute](How_to_contribute.md)
</details>
<br>
<details>
<summary>   

## The Web-/ Desktop-App</summary>
The Web- and Desktop-App are nearly identic, except for the framework used and some UI.

They are both used as client UI applications to send requests to the server, the core. They should support everything the server supports but don't do anything without compliance with the server. They are just the UI, not the actually thinking thing.

### Language
Web:     JavaScript, HTML, CSS<br>
Dekstop: JavaScript (Node.js), HTML, CSS

### Complexity
```
0 |----+----| 10
       5
```

Mainly caused by the connection between the Desktop- and the Web-App, as you have to get everything updated for **both** of these.
<br>Also, the start and thus simpliest part is partly already done.

### Finished
```
0 |---+-----| 10
      4
```

### If you got interested,<br>
### [Read more](https://codeberg.org/Jikango/pages/wiki)
</details>
<br>
<details>
<summary>   

## The Android App</summary>
It should be an App which is as much as possible the same as the Web-App but optimised for Android, so not just a Webview of the website.

### Language
Flutter / Dart

### Complexity
```
0 |----?----| 10
```

You will have to ask Blackeleven or just start.

### Finished
```
0 |---------| 10
  0
```

### If you got interested,<br>
### You will have to [get started](How_to_edit.md) and [create it](How_to_contribute.md).
</details>
<br>
<details>
<summary>   

## The Frontier</summary>
The frontier should be a server getting all the requests and evaluating them in security terms, for example spam, unregistered, whatever, and sending them through at a as much as possible local network to the API server.

### Language
Unknown

### Complexity
```
0 |----?----| 10
```

You will have to develop everything yourself, from the plan and the start to the running server. For sure, if you use a language that is known to me, I can help you.

### Finished
```
0 |---------| 10
  0
```

### If you got interested,<br>
### You will have to [get started](How_to_edit.md) and [create it](How_to_contribute.md).
</details>
<br>
<details>
<summary>   

## The API</summary>
It gets the requests and is the actual thinking core. It sets everything and is the you could say manager. However, it is in the first place not there for security. The only thing it would do for security is not hand out the password, encrypt the password or similar and maybe, depending on the frontier setup, check if the user token is correct.

### Language
JavaScript

### Complexity
```
0 |-------+-| 10
          8
```

It is the core, so doing everything and since it is unfinished yet and still missing the patch fix, it will be a long and tough way.

### Finished
```
0 |----+----| 10
       5
```

### If you got interested,<br>
### [Read more](https://codeberg.org/Jikango/Database_API/wiki)
<br>

If you want to contribute, continue with these two:
- [How to edit](How_to_edit.md)
- [How to contribute](How_to_contribute.md)
</details>
<br>
<details>
<summary>   

## The Database</summary>
Just a simple MySQL Database which hosts the schools, classes and everything else. You could call it just the storage hall or document folder.

### Language
MySQL

### Complexity
```
0 |-+-------| 10
    2
```

Built into the Database API, so if you want to only work on this, you may have to team up with someone doing the JS Part.

### Finished
```
0 |-----+---| 10
        6
```

### If you got interested,<br>
### [Read more](https://codeberg.org/Jikango/Database_API/wiki)
<br>

If you want to contribute, continue with these two:
- [How to edit](How_to_edit.md)
- [How to contribute](How_to_contribute.md)
</details>
<br>
<details>
<summary>   

## The commons frame</summary>
Packed with utils and images, it is the most low and most often used code.

### Language
JavaScript, JavaScript (Node.js), (JavaScript (Neutralino), partly coded but not public)

### Complexity
```
0 |--+------| 10
     3
```

It is not that difficult as it is just what you would do when you start with programming and want to get a great app working. Though you have to think about it as plugins.

### Finished
```
0 |------+--| 10
         7
```
However, there are always new things to come and it grows with the developement of other repositories.

### If you got interested,<br>
### [Read more](https://codeberg.org/Jikango/commons/wiki)
<br>

If you want to contribute, continue with these two:
- [How to edit](How_to_edit.md)
- [How to contribute](How_to_contribute.md)
</details>

---

# [Contact](Contact.md)
