# Links
to help you

<details><summary>

### Jikango</summary>
- https://codeberg.org/Jikango

---

</details>
<details><summary>

### Development programs</summary>
- https://git-scm.com/downloads

#### DB API
- https://nodejs.org/en/download/
- https://dev.mysql.com/downloads/installer/

---

</details>
<details><summary>

### Database</summary>
- https://www.w3schools.com/nodejs/nodejs_mysql.asp
- https://www.w3schools.com/mysql/mysql_insert.asp
- https://dev.mysql.com/doc/
- https://www.npmjs.com/package/mysql
- https://www.npmjs.com/package/mysql2
- https://www.mysqltutorial.org/mysql-nodejs/select/

---

</details>
<details><summary>

### API</summary>
- https://www.geeksforgeeks.org/what-is-rest-api-in-node-js/
- https://nodejs.org/dist/latest-v8.x/docs/api/
- https://jsdoc.app/

#### React Router
- https://reactrouter.com/en/main/start/overview
- https://dev.to/andrewbaisden/node-and-react-router-dynamic-api-routes-21mg
- https://expressjs.com/en/4x/api.html#router
- https://www.w3schools.com/react/react_router.asp

#### WebUntis API
- https://help.untis.at/hc/de/articles/4886785534354-API-documentation-for-integration-partners

#### Coming
- https://discord.com/developers/docs/reference#snowflakes
- https://github.com/twitter-archive/snowflake/tree/snowflake-2010/src/main

---

</details>
<details><summary>

### Requests</summary>
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#information_responses (Response Codes)

---

</details>
<details><summary>

### WebApp</summary>
#### Router
- https://github.com/flatiron/director
- If to slow: https://www.jsviews.com/#jsviews
#### Extra Framework?
- https://svelte.dev/

---

</details>
<details><summary>

### Android</summary>
#### Example Projects
- https://github.com/Perflyst/OpenUntis
- https://github.com/flowhorn/schulplaner

---

</details>
<details><summary>

### RegEx</summary>
- https://www.regextester.com/15
- https://regex101.com/library/
- https://regex101.com/r/tA9pM8/1

---

</details>
<details><summary>

### For Questions</summary>
- https://learnxinyminutes.com/
- https://stackoverflow.com/
- https://developer.mozilla.org/
- https://www.w3schools.com/
- https://www.geeksforgeeks.org/
- [Contact](Contact.md)⁠
- https://dev.to/
- https://ecosia.org/ or for more complex questions https://google.com/

---

</details>
<details><summary>

### FAQ</summary>
- Is the variable existing in any form?<br>https://stackoverflow.com/questions/5113374/javascript-check-if-variable-exists-is-defined-initialized
- What is the difference between == and ===?<br>https://stackoverflow.com/questions/359494/which-equals-operator-vs-should-be-used-in-javascript-comparisons

---

</details>

---

# [Contact](Contact.md)