# Activate 2FA in Codeberg
1. Click on your profile (top right)
2. Go to the settings
3. Select Security
4. Click on "Activate Two-Factor-Authentication"
5. Scan the Code with a 2FA App (for ex. [Aegis](https://getaegis.app/)) and save it
6. Enter the shown code it the field shown in codeberg
7. Save your scratch token in an as most possible file or similar
<img src='https://cdn.discordapp.com/attachments/1049380778510909481/1049380968487723048/image.png'>

# Push and commit with 2FA
1. Click on your profile (top right)
2. Go to the settings
3. Select "Applications"
4. Enter some name in the input
5. Click on "Generate Token"
6. Copy and save the token appearing in the top into an as most as possible file or similar
7. Enter the token into the password field instead of your password when commiting. The user name is your username
<img src="https://cdn.discordapp.com/attachments/1049380778510909481/1049381064948338728/image.png">

# Search ToDos with RegEx in VS Code
1. Click on search
2. Activate RegEx
3. Enter (TODO|FIX)((?!COMING).)*$ as search, for other regexes you can use https://www.regextester.com/
(You may have to activite case-sensitivity)
<img src='https://cdn.discordapp.com/attachments/1049380778510909481/1057768516444487772/image.png'>

# Add descriptions to commits
1. Enter the title in the commit field
2. Press Enter
3. Enter the description in the commit field

# How `f_ids` are working
The base structure and index how the are returned from index (from 15/2/2023)

```
localhost/lesson/1:1,c:13_2,u:1,r:1
                  ^    ^     ^   ^
                  |    |     |   Repetitions, Index 2
                  |    |     Users, their classes are gotten and added to classes, Index 1
                  |    Classes, directly added to Index 1
                  The ID/Time of the lesson, Index 0
```
Also `<` can be used as splitter:
`localhost/lesson/1:1<c:13_2<u:1<r:1`

# Send requests to the DB API via curl
curl+
1. Request type: GET, PATCH, PUT or DELETE per -X
2. Header per -H
3. Escaped Request Body per -d
4. Endpoint
5. And you should have gotten something like this:
```
curl -X GET -H "Content-Type: application/json" -H "Accept: application/json" -d "{\"content\":{\"type\":\"CLASS\",\"id\":\"13/2\",\"db_id\":\"school of time\"}}" "http://localhost:8080/" 
```