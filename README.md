# Status and Contribution

## Welcome to this Repository!
Nice that you take a look at this.<br>
As this project is in the current status not developed by it's founders, **you got the power to change the world**. (At least the one of time)

### So, this repository will show you, how and what you can do!

For easier access, the pages are built in in the wiki and as files here, so choose what fits you best.<br>

### [The wiki](https://codeberg.org/Jikango/Status_and_Contribution/wiki)

## I am really thankful for everyone that plans or contributes to this big project!

### So, let's dive into the [basics](Jikango_Basics.md).